/**
 * A MarkitUp editor implementation.
 *
 * @type {editorBase}
 */
$.fn.jinplace.editors['extra:markitup'] = {
	blurAction: 'ignore',
        //blurAction: 'submit',

        activate: function(form, field) {
            var myTextileSettings = {
                nameSpace:           "textile", // Useful to prevent multi-instances CSS conflict
                previewParserPath:   "~/sets/textile/preview.php",
                onShiftEnter:        {keepDefault:false, replaceWith:'\n\n'},
                markupSet: [
                    {name:'Heading 1', key:'1', openWith:'h1(!(([![Class]!]))!). ', placeHolder:'Your title here...' },
                    {name:'Heading 2', key:'2', openWith:'h2(!(([![Class]!]))!). ', placeHolder:'Your title here...' },
                    {name:'Heading 3', key:'3', openWith:'h3(!(([![Class]!]))!). ', placeHolder:'Your title here...' },
                    {name:'Heading 4', key:'4', openWith:'h4(!(([![Class]!]))!). ', placeHolder:'Your title here...' },
                    {name:'Heading 5', key:'5', openWith:'h5(!(([![Class]!]))!). ', placeHolder:'Your title here...' },
                    {name:'Heading 6', key:'6', openWith:'h6(!(([![Class]!]))!). ', placeHolder:'Your title here...' },
                    {name:'Paragraph', key:'P', openWith:'p(!(([![Class]!]))!). '}, 
                    {separator:'---------------' },
                    {name:'Bold', key:'B', closeWith:'*', openWith:'*'}, 
                    {name:'Italic', key:'I', closeWith:'_', openWith:'_'}, 
                    {name:'Stroke through', key:'S', closeWith:'-', openWith:'-'}, 
                    {separator:'---------------' },
                    {name:'Bulleted list', openWith:'(!(* |!|*)!)'}, 
                    {name:'Numeric list', openWith:'(!(# |!|#)!)'}, 
                    {separator:'---------------' },
                    {name:'Picture', replaceWith:'![![Source:!:http://]!]([![Alternative text]!])!'}, 
                    {name:'Link', openWith:'"', closeWith:'([![Title]!])":[![Link:!:http://]!]', placeHolder:'Your text to link here...' },
                    {separator:'---------------' },
                    {name:'Quotes', openWith:'bq(!(([![Class]!]))!). '}, 
                    {name:'Code', openWith:'@', closeWith:'@'}, 
                    {separator:'---------------' },       
                    {name:'Preview', call:'preview', className:'preview'}
                ]
            };
            
            field.markItUp(myTextileSettings).focus();
            
            var container = field.closest("*[data-jin='1']");          
            
            $(document).one('click', function(e) {
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
                {
                    var content = JSON.stringify(field.tokenInput('get'));
                    
                    field.tokenInput('destroy');    // remove tokeninput
                    field.val(content); // set new content
                    form.submit();  // submit jinplace form and remove
                }
            });
        },
        
        onResult: function(results){
            var values = (new Function("return " + results))();
            return values;
        }
};

