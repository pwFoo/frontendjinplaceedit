/**
 * A tokeninput implementation.
 *
 * @type {editorBase}
 */
$.fn.jinplace.editors['extra:tokeninput'] = {
	blurAction: 'ignore',
        //blurAction: 'submit',

        activate: function(form, field) {
            var elem = form.parent();
            var url  = elem.attr('data-loadurl') + elem.attr('data-querypath');
            var values = (new Function("return " + field.val()))();
            
            field.tokenInput(url, {
                //theme: 'mac',
                prePopulate: values,
                searchDelay: 700,
                minChars: 2,
                preventDuplicates: true,    // not add duplicates!
                excludeCurrent: true,       // hide duplicates in search list
                allowFreeTagging: true      // allow not existing tags
            }).focus();
            
            var container = field.closest("*[data-jin='1']");          
            
            $(document).one('click', function(e) {
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
                {
                    var content = JSON.stringify(field.tokenInput('get'));
                    
                    field.tokenInput('destroy');    // remove tokeninput
                    field.val(content); // set new content
                    form.submit();  // submit jinplace form and remove
                }
            });
        },
        
        onResult: function(results){
            var values = (new Function("return " + results))();
            return values;
        }
};