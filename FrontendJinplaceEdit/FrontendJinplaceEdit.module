 <?php
/**
 * Processwire 'FrontendJinplaceEdit' module
 * 
 * Frontend inline edit via jEditable jQuery plugin
 * 
 * @author pwFoo
 * @since 2014-12-07
 * 
 * ProcessWire 2.x
 * Copyright (C) 2011 by Ryan Cramer
 * Licensed under GNU/GPL v2, see LICENSE.TXT
 *
 * http://www.processwire.com
 * http://www.ryancramer.com
 */
class FrontendJinplaceEdit extends WireData implements Module {
    /**
     * getModuleInfo is a module required by all modules to tell ProcessWire about them
     * @return array
     */
    public static function getModuleInfo() {
        return array(
            'title' => 'FrontendJinplaceEdit',
            'summary' => 'Frontend inline edit via Jinplace jQuery plugin',
            'version' => '0.0.2',
            'installs' => array("FrontendJinplaceEditProcess"),
        );
    } 
    
    /** @var string Process / admin page name */
    const pageName = "jinplace";
    
    private $processUrl;
    
    private $type = array(
        // [0] => data-url, [1] => data-loadurl
        //'text' => array(null, null),
        //'textarea' => array(null, null),
        'select' => array(null, 'selectlist/'),
        //'extra:checkbox' => array(null, null),
        //'extra:checkbox_demo' => array(null, null),
        'extra:tokeninput' => array('tokeninput/', 'tokeninputlist/', true),
    );
    
    /** @var array Default jinplace data options */
    private $options = array();
    
    public function init() {
        $adminPage = self::pageName;
        $this->processUrl = "{$this->config->urls->admin}page/{$adminPage}/";
        $this->options = array(
            'data-okButton'    => $this->_('OK'),
            'data-cancelButton'=> $this->_('Cancel'),
        );
    }
    
    public function scripts() {
        $this->config->styles->add($this->config->urls->FrontendJinplaceEdit . 'css/token-input.css');
        $this->config->styles->add($this->config->urls->FrontendJinplaceEdit . 'css/token-input-facebook.css');
        $this->config->styles->add($this->config->urls->FrontendJinplaceEdit . 'css/token-input-mac.css'); 
        
        $this->config->scripts->add($this->config->urls->FrontendJinplaceEdit . 'js/jinplace-v1.1.0.min.js');
        $this->config->scripts->add($this->config->urls->FrontendJinplaceEdit . 'js/jinplace-extra.min.js');
        $this->config->scripts->add($this->config->urls->FrontendJinplaceEdit . 'js/jquery.tokeninput.min.js');
        $this->config->scripts->add($this->config->urls->FrontendJinplaceEdit . 'js/jinplace.tokeninput.js');
//        $this->config->scripts->add($this->config->urls->FrontendJinplaceEdit . 'js/jinplace.markitup.js');
        $this->config->scripts->add($this->config->urls->FrontendJinplaceEdit . 'js/FrontendJinplaceEdit.js');
    }
    
    public function setup($field = null, $refPage = null, $options = null) {
        if (!$field || !$refPage instanceof Page || !$refPage->editable($field)) return null;

        $options['data-jin'] = 1;
        $options['data-object'] = $refPage->id;
        $options['data-attribute'] = $field;
        
        $this->setDataUrls($options);
                
        foreach (array_merge($this->options, $options) as $key => $value) {
            $data[] = "{$key}='{$value}'";
        }
        
        return implode(' ', $data);
    }
    
    private function setDataUrls ($data = null) {
        if ((isset($data['data-type'])) && $key = in_array($data['data-type'], $data)) {
            $key = $data['data-type'];
            
            $this->options['data-url'] = $this->processUrl . $this->type[$key][0];
            
            if (isset($this->type[$key][1])) {
                $this->options['data-loadurl'] = $this->processUrl . $this->type[$key][1];
            }
            if (isset($this->type[$key][2])) {
                $this->options['data-querypath'] = "{$data['data-object']}/{$data['data-attribute']}/";
            }
        }
        else {
            $this->options['data-url'] = $this->processUrl;
        }
    }
}