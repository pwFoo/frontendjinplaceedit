Simple proof of concept jQuery Jinplace plugin module to update text field, textarea, page reference select, page reference tokeninput (free-)tagging, labeled and unlabeled checkbox.

Download and install the module. Process page will be created (and deleted during uninstall).

example template uses following fields
- title
- body 
- checkbox (used twice!)
- tags (page reference, tagging, also create new tags)
- selectlist (page reference, single value to use as single select)

You have to add and configure the fields before testing...


!!! urlSegements need to be activated for admin template !!!


Example template file

~~~~
<!DOCTYPE html>
<html lang="en">
	<head>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title><?php echo $page->title; ?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo $config->urls->templates?>styles/main.css" />
            <?php 
                $fie = $modules->get('FrontendJinplaceEdit');
                $modules->get('JqueryCore');
                $fie->scripts();

                $select = array('data-type' =>'select');
                $tokeninput = array('data-type' =>'extra:tokeninput');
                $textarea = array('data-type' =>'textarea', 'data-textOnly' => false);
                $checkbox = array('data-type' =>'extra:checkbox', 'data-okButton' => null, 'data-cancelButton' => null);
                $checkbox_label = array('data-type' =>'extra:checkbox_demo', 'data-okButton' => null, 'data-cancelButton' => null);
    
                foreach ($config->styles as $file) { echo "<link type='text/css' href='$file' rel='stylesheet' />\n"; }
                foreach ($config->scripts as $file) { echo "<script type='text/javascript' src='$file'></script>\n"; } 
                ?>
	</head>
	<body>
            <h1 <?=$fie->setup('title', $page)?>><?=$page->title?></h1>
            <div <?=$fie->setup('body', $page, $textarea)?>><?=$page->body?></div>
            <div title="without label..." <?=$fie->setup('checkbox', $page, $checkbox)?>><?=($page->checkbox ? 'Yes' : 'No')?></div>
            <div title="with label..." <?=$fie->setup('checkbox', $page, $checkbox_label)?>><?=($page->checkbox ? 'Yes' : 'No')?></div>
            <div <?=$fie->setup('tags', $page, $tokeninput)?>><?=$page->tags->implode(', ', 'title')?></div>
            <div <?=$fie->setup('selectlist', $page, $select)?>><?=($selected = $page->SelectList ? $page->selectlist->title : 'empty')?></div>
	</body>
</html>
~~~~